import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      one: 0,
      two: 0,
      result: 0
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.recursion = this.recursion.bind(this)
  }

  handleChange(event) {
    const target = event.target;    
    const name = target.name;

    this.setState({ 
      [name]: event.target.value 
    });
  }

  recursion(one, two){
      if(two === 0){
        return Math.abs(one);
      } else {
        return this.recursion(two, one % two)
      }
  }

  handleSubmit(event) {
    const { one, two } = this.state;    

    this.setState({
      result: this.recursion(one, two),
      one: 0,
      two: 0
    })

    event.preventDefault();
  }

  render() {
    const { result } = this.state;
    return (
      <div className="App">

        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Result: {result}</h2>
        </div>   

        <div className="container">
          <div className="row"> 
            <div className="col-md-8 col-md-offset-2">

            <form className="form-inline" onSubmit={this.handleSubmit} style={{padding: '20px 0px'}}>

              <div className="form-group input-group col-md-6" style={{padding: '0px 5px'}}>
                <span className="input-group-addon text-uppercase">First</span>
                <input id="msg" type="number" className="form-control" name="one" value={this.state.one} onChange={this.handleChange}/>
              </div>

              <div className="form-group input-group col-md-6" style={{padding: '0px 5px'}}>              
                <input id="msg" type="number" className="form-control" name="two" value={this.state.two} onChange={this.handleChange}/>
                <span className="input-group-addon text-uppercase">Second</span>
              </div>

              <div className="btn-group" style={{paddingTop: '20px'}}>
                <input type="submit" className="btn btn-primary" value="CALCULATE" />            
              </div>   

            </form>

            </div>  
          </div>   
        </div>      
      </div>
    );
  }
}

export default App;
